import { Component, Event, EventEmitter, h } from '@stencil/core';

@Component({
  tag: 'my-component',
  shadow: true,
})
export class MyComponent {
  @Event() myEvent: EventEmitter<string>;

  render() {
    return (
      <div>
        <button onClick={() => this.myEvent.emit('Hello World')}>Click me to emit an event</button>
      </div>
    );
  }
}