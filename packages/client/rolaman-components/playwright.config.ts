import { defineConfig, expect } from '@playwright/test';
import { matchers } from 'stencil-playwright';

expect.extend(matchers);


export default defineConfig({ 
  testMatch: 'tests/my-component.e2e.ts',
  use: {
    baseURL: 'http://localhost:3333'
  },
  webServer: {
    command: 'serve -p 3333',
    port: 3333,
    reuseExistingServer: !process.env.CI
  }
});