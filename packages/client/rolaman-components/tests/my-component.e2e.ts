import { test } from "stencil-playwright";
import { expect } from "@playwright/test";

test.describe("my-component", () => {
  test("it should render a button", async ({ page }) => {
    await page.setContent(`<my-component></my-component>`);

    const button = await page.locator("my-component button");

    await expect(button).toBeVisible();
  });

  test("it should emit an event", async ({ page }) => {
    await page.setContent(`<my-component></my-component>`);

    const myEventSpy = await page.spyOnEvent("myEvent");

    const button = await page.locator("my-component button");
    await button.click();

    expect(myEventSpy).toHaveReceivedEvent();
  });

  test("it should emit an event with the expected detail", async ({ page }) => {
    await page.setContent(`<my-component></my-component>`);

    const myEventSpy = await page.spyOnEvent("myEvent");

    const button = await page.locator("my-component button");
    await button.click();

    await myEventSpy.next();

    expect(myEventSpy).toHaveReceivedEventDetail("yooo");
  });
});