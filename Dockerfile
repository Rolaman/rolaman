FROM node:16.17.0-bullseye-slim
WORKDIR /app
COPY . /app
RUN npm install
EXPOSE 3000
CMD "npm" "start"